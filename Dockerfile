FROM ubuntu:15.04
MAINTAINER Pavel Ovsiannikov <mail@pavovs.ru>

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update

RUN apt-get install -y php5-cli php5-fpm php5-mcrypt php5-curl php5-pgsql php5-memcached php5-xdebug \
    php5-dev memcached nginx gcc

ADD mime.types /etc/nginx/mime.types
ADD default /etc/nginx/sites-available/default
ADD nginx.conf /etc/nginx/nginx.conf
ADD php.ini /etc/php5/fpm/php.ini
ADD php.ini /etc/php5/cli/php.ini
ADD www.conf /etc/php5/fpm/pool.d/www.conf
ADD xdebug.ini /etc/php5/mods-available/xdebug.ini
ADD phalcon-v1.3.4.tar.gz /usr/local/src
ADD startup /usr/bin/startup

RUN chmod +x /usr/bin/startup

RUN cd /usr/local/src/cphalcon-phalcon-v1.3.4/build && ./install; \
    echo "extension=phalcon.so" > /etc/php5/mods-available/phalcon.ini; \
    php5enmod phalcon

RUN apt-get -y autoremove; apt-get -y autoclean; apt-get -y clean

RUN rm -rf /var/www/* /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/local/src/cphalcon-phalcon-v1.3.4
ENV DEBIAN_FRONTEND dialog

VOLUME /var/www
EXPOSE 80

ENTRYPOINT ["startup"]